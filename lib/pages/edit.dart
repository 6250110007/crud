import 'package:crud_mysql_flutter/models/products.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Edit extends StatefulWidget {
  final Products product;

  Edit({required this.product});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  // This is for text to edit
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request
  Future editProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/update_product.php"),
      body: {
        "pid": widget.product.pid.toString(),
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await editProducts();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.product.name);
    priceController =
        TextEditingController(text: widget.product.price.toString());
    descriptionController =
        TextEditingController(text: widget.product.description);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Product"),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text('Save'),
          icon: Icon(Icons.save),
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
          height: MediaQuery.of(context).size.height * 0.35,
          width: MediaQuery.of(context).size.width * 1,
          child: Card(
            color: Colors.grey[300],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            elevation: 8,
            child: Container(
              child: (Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  text(
                    nameController: nameController,
                    label: "Product Name",
                    hint: "Enter Product Name",
                  ),
                  SizedBox(height: 10.0),
                  text(
                    nameController: priceController,
                    label: "Price",
                    hint: "Enter product price",
                  ),
                  SizedBox(height: 10.0),
                  text(
                    nameController: descriptionController,
                    label: "Description",
                    hint: "Enter product description",
                  ),
                ],
              )),
            ),
          ),
        ),
      ),
    );
  }
}

class text extends StatelessWidget {
  const text({
    Key? key,
    required this.nameController,
    required this.label,
    required this.hint,
  }) : super(key: key);

  final TextEditingController nameController;
  final label;
  final hint;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, right: 25, top: 5),
      child: Container(
          child: TextField(
        controller: nameController,
        decoration: InputDecoration(
          labelText: label,
          hintText: hint,
        ),
      )),
    );
  }
}
